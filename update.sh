#!/bin/bash
cd $1
if [ -d .git ]; then
    git fetch --all
    git reset --hard origin/master
else
  exit 1
fi

gitstring=$(git log -1 --pretty=%B)
if [[ $gitstring == *"[RESTART]"* ]]; then
  /bin/bash ~/restart.sh
fi

gitstring=$(git log -1 --pretty=%B)
if [[ $gitstring == *"Merge branch"* ]]; then
        gitstring=$(git log -2 --pretty=%B)
        if [[ $gitstring == *"[RESTART]"* ]]; then
          /bin/bash ~/restart.sh
        fi
fi
if [-f $2]; then
  /bin/bash $2 $1 --dontask --snapshot-stable
fi
